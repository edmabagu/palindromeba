package com.palindrome;

import com.palindrome.model.Palindrome;
import com.palindrome.service.PalindromeService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;


/**
 * Created by EdmundasB on 2017.02.15.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:WEB-INF/palindrome-servlet.xml" })
public class PalindromeServiceTest {

    @Autowired
    private PalindromeService palindromeService;

    /**
     * Tests if given string palindrome
     */
    @Test
    public void isPalindromeTest(){
        Palindrome palindrome = new Palindrome("Animal loots foliated detail of stool lamina");
        boolean palindromeCheck  = palindromeService.isStringPalindrome(palindrome);
        assertTrue(palindromeCheck);
    }

    /**
     * Tests if given string not palindrome
     */
    @Test
    public void isNotPalindromeTest(){
        Palindrome palindrome = new Palindrome("Animal loots foliated detail of stool");
        boolean palindromeCheck  = palindromeService.isStringPalindrome(palindrome);
        assertFalse(palindromeCheck);
    }

    /**
     * Tests if given string has not aplhanumeric symbols
     */
    @Test(expected = IllegalArgumentException.class)
    public void hasNotAlphanumericChars(){
        Palindrome palindrome = new Palindrome(null);
        boolean palindromeCheck  = palindromeService.isStringPalindrome(palindrome);
    }



}
