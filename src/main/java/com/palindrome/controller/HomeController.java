package com.palindrome.controller;

import com.palindrome.model.Palindrome;
import com.palindrome.model.PalindromeResult;
import com.palindrome.service.PalindromeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by EdmundasB on 2017.02.13.
 */
@Controller
public class HomeController {

    protected static final String VIEW_NAME_HOMEPAGE = "index";
    protected static final String VIEW_NAME_RESULT = "result";

    protected static final String MODEL_NAME_PALINDROME_DTO= "palindrome";
    protected static final String MODEL_NAME_PALINDROME_RESULT_DTO= "palindromeResult";

    @Autowired
    private PalindromeService palindromeService;

    /**
     * Home page controller
     * @param request
     * @param model
     * @return
     */
    @RequestMapping(value="/index", method = RequestMethod.GET)
    public String helloWorld(WebRequest request, Model model) {

        model.addAttribute(MODEL_NAME_PALINDROME_DTO, new Palindrome());

        return VIEW_NAME_HOMEPAGE;
    }

    /**
     * Palindrome checker, checking if given string palindrome,
     * redirects to result page.
     * @param palindrome
     * @param result
     * @param model
     * @return
     */
    @RequestMapping(value = "/check", method = RequestMethod.POST)
    public String saveOrUpdateUser(@ModelAttribute("palindrome") Palindrome palindrome, BindingResult result, Model model) {

        boolean checkResult = palindromeService.isStringPalindrome(palindrome);

        PalindromeResult palindromeResult = new PalindromeResult();
        palindromeResult.setInputText(palindrome.getInputText());
        palindromeResult.setResult(checkResult);

        model.addAttribute(MODEL_NAME_PALINDROME_RESULT_DTO, palindromeResult);

        return VIEW_NAME_RESULT;
    }
}
