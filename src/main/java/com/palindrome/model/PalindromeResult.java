package com.palindrome.model;

/**
 * Created by EdmundasB on 2017.02.14.
 */
public class PalindromeResult {

    private String inputText;

    private boolean result;

    public PalindromeResult() {
    }

    public String getInputText() {
        return inputText;
    }

    public void setInputText(String inputText) {
        this.inputText = inputText;
    }

    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }
}
