package com.palindrome.model;

/**
 * Created by EdmundasB on 2017.02.13.
 */
public class Palindrome {

    private String inputText;

    public Palindrome(String inputText) {
        this.inputText = inputText;
    }

    public Palindrome() {
    }


    public String getInputText() {
        return inputText;
    }

    public void setInputText(String inputText) {
        this.inputText = inputText;
    }

    @Override
    public String toString() {
        return "Palindrome{" +
                "inputText='" + inputText + '\'' +
                '}';
    }
}
