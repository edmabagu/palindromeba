package com.palindrome.service;

import com.palindrome.model.Palindrome;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

/**
 * Created by EdmundasB on 2017.02.13.
 */
@Service
public class PalindromeServiceImpl implements PalindromeService{

    public boolean isStringPalindrome(Palindrome palindrome) {
       String text = palindrome.getInputText();

        if (text == null) {
            throw new IllegalArgumentException("Input is null");
        }
        String chars = text.replaceAll("[^a-zA-Z]", "").toLowerCase();

        int left = 0;
        int right = chars.length() - 1;
        while (left < right) {
            if (chars.charAt(left++) != chars.charAt(right--)) {
                return false;
            }
        }
        return true;
    }

}
