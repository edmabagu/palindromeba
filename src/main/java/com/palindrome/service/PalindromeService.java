package com.palindrome.service;

import com.palindrome.model.Palindrome;

/**
 * Created by EdmundasB on 2017.02.13.
 */
public interface PalindromeService {

    /**
     * Checks if input string  null,
     * if not checks if given string palindrome, returns true if palindrome.
     * @param palindrome
     * @return
     */
    public boolean isStringPalindrome(Palindrome palindrome);

}
