<%--
  Created by IntelliJ IDEA.
  User: EdmundasB
  Date: 2017.02.14
  Time: 16:16
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>result</title>
</head>
<body>
<br>
<div style="text-align:center">
    <h2>
        Palindrome checker<br> <br>
    </h2>
    <c:choose>
        <c:when test="${palindromeResult.result}">
            <b> ${palindromeResult.inputText} </b> is palindrome
        </c:when>
        <c:otherwise>
            <b> ${palindromeResult.inputText} </b> is not palindrome
        </c:otherwise>
    </c:choose>
</div>
</body>
</html>
