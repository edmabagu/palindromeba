<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<html>
<head>
    <title>Palindrome calculator</title>
</head>
<script>
    jQuery(document).ready(function($) {

        $("#search-form").submit(function(event) {

            // Disble the search button

            // Prevent the form from submitting via the browser.
            event.preventDefault();

            searchViaAjax();

        });

    });

    function searchViaAjax() {

        var booking = {}
        booking["plannerEventId"] = "aa";
        booking["customerId"] = "aa";
        booking["providerId"] = "aa";
        booking["posterId"] = "aa";
        booking["message"] = "aa";

        $.ajax({
            type : "POST",
            contentType : "application/json",
            url : "/bookPlannedEvent",
            data : JSON.stringify(booking),
            dataType : 'json',
            timeout : 100000,
            success : function(data) {
            },
            error : function(e) {
            },
            done : function(e) {
            }
        });
    }

    function display(data) {
        var json = "<h4>Ajax Response</h4><pre>"
            + JSON.stringify(data, null, 4) + "</pre>";
        $('#feedback').html(json);
    }
</script>

<body>
<br>

<body>
<br>
<div style="text-align:center">
    <h2>
        Palindrome checker<br> <br>
    </h2>
    <form:form id="createPosterForm" action="${pageContext.request.contextPath}/check" modelAttribute="palindrome" method="POST" enctype="utf8"  role="form">
        <div class="form-group">
            <label for="palindrome">Enter palindrome</label>
            <form:input type="text" path="inputText" class="form-control"  id="palindrome" placeholder=""/>
            <button type="submit" id="check_button">Check</button>
        </div>
    </form:form>
</div>
</body>
</body>
</html>
